// @flow
import { combineReducers } from 'redux';
import { authReducer as auth } from './auth';
// import type { TipsStateType } from './tips/reducer';

const initialState = {};

const rootReducer = (state: any = initialState, action: any = {}) => {
    const appReducer = combineReducers({
        auth,
    });

    return appReducer(state, action);
};

// export type StateType = {|
//     tips: TipsStateType,
// |};

export default rootReducer;

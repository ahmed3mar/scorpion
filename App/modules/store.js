// @flow
import { applyMiddleware, createStore, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native

import { AsyncStorage } from 'react-native';
import reducers from './reducers';
import rootSaga from './sagas';

const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(persistConfig, reducers)

const configureStore = () => {
    const sagaMiddleware = createSagaMiddleware();
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const middlewares = [sagaMiddleware];
    const enhancers = [applyMiddleware(...middlewares)];//, persistReducer(persistConfig)];
    return { ...createStore(persistedReducer, composeEnhancers(...enhancers)), runSaga: sagaMiddleware.run };
};

export default (): Promise<any> =>
    new Promise(resolve => {
        const store = configureStore();
        // store.runSaga(rootSaga);
        // return resolve({store})
        return persistStore(
            store,
            null,
            // {
            //     storage: AsyncStorage,
            //     //whitelist: ['auth'], // handy when debugging
            //     blacklist: ['navigation', 'sync'],
            // },
            () => resolve({ store })
        );
    });

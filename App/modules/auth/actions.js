export function logIn({ email, password }) {
    return {
        type: 'LOG_IN',
        email,
        password,
    };
}

export function setLogin(user) {
    return {
        type: 'SET_LOGIN',
        user,
    };
}

export function logOut() {
    return {
        type: 'LOG_OUT',
    };
}

export function register({ username, password }) {
    return {
        type: 'REGISTER',
        username,
        password,
    };
}

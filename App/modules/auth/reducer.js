const initialState = {
    logged_in: true,
    user: {
        username: '',
        image: '',
        id: '',
    },
    token: '',
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SET_LOGIN':
            return { ...state, open: !state.open };
        case 'OPEN_MENU':
            return { ...state, open: true };
        case 'CLOSE_MENU':
            return { ...state, open: false };
        default:
            return state;
    }
};
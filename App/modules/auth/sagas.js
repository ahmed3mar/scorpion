// @flow
import { call, put, takeEvery } from 'redux-saga/effects';
import {setLogin} from "./actions";
// import { resetStore, putTokens } from './actions';
// import { Scrumble, Analytics } from '../../services';
// import Navigation from '../../services/Navigation';

function* doLogin(action) {
    console.log("SSS")
    // if (action.type !== 'LOGIN') return;
    // try {
    //     const scrumbleToken = yield call(Scrumble.login, action.payload.trelloToken);
    //     yield put(putTokens(action.payload.trelloToken, scrumbleToken));
    //     Navigation.redirectAfterLogin(true);
    // } catch (error) {
    //     console.warn('[saga] login', error);
    //     Navigation.resetToLogin();
    //     Alert.alert('Sorry!', 'There has been an error when trying to log you on Scrumble. ' + error);
    // }

    // request
    yield put(setLogin({
        id: 1,
    }));
}

export default function*(): Generator<*, *, *> {
    yield* [takeEvery('LOG_IN', doLogin)];
}

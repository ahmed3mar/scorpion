import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, AsyncStorage} from "react-native";
import { Input, CardItem , Card , Button , Story , ActivityCard} from '../../components/index'
import Header from "../FriendsScreen/Header";
import axios from "axios/index";
// import { logIn } from "../../modules/auth";

// @connect( state => ({
//
// }), () => ({
//     logIn: logIn,
// }) )

const IMAGE_SOURCE = require('../../assets/images/person1.jpg')
const HAPPY = require('../../assets/images/happy.jpg')
const SAD = require('../../assets/images/sad.jpg')
const ANGRY = require('../../assets/images/angry.png')
const LOVE = require('../../assets/images/love.jpg')
const mood1 = require('../../assets/images/mood1.jpg')
const profile1 = require('../../assets/images/person1.jpg')
const mood2 = require('../../assets/images/mood2.jpg')
const profile2 = require('../../assets/images/person2.jpg')
const mood3 = require('../../assets/images/mood3.jpg')
const profile3 = require('../../assets/images/person3.png')
const mood4 = require('../../assets/images/mood4.jpg')
const profile4 = require('../../assets/images/person4.png')

export default class HomeScreen extends React.Component {

    static propTypes = {
        navigation: PropTypes.object,
    };

    static navigationOptions = {
        // title: 'Gift',
        header: null,
    };

    constructor(props) {
        super(props);

        this.state = {
            activities: [
                {
                    id: 1,
                    name: 'AHmed',
                    email: 'Ahmed@gg',
                    'image': '',
                    'activities': {
                        'gift': {
                            id: 1,
                            send_from: 1,
                        }
                    },
                    type: 'wow'
                },
                {
                    id: 1,
                    name: 'AHmed',
                    email: 'Ahmed@gg',
                    'image': '',
                    'activities': {
                        'gift': {
                            id: 1,
                            send_from: 1,
                        }
                    },
                    type: 'wow'
                },{
                    id: 1,
                    name: 'sdsd',
                    email: 'Ahmed@gg',
                    'image': '',
                    'activities': {
                        'gift': {
                            id: 1,
                            send_from: 1,
                        }
                    },
                    type: 'wow'
                },{
                    id: 1,
                    name: 'AHmed',
                    email: 'Ahmed@gg',
                    'image': '',
                    'activities': {
                        'gift': {
                            id: 1,
                            send_from: 1,
                        }
                    },
                    type: 'wow'
                }
            ]
        }
    }

    async componentWillMount() {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value !== null) {
                // We have data!!
                this.setState({
                    user: JSON.parse(value)
                }, () => {
                    this.getActivities();
                })
            }
        } catch (error) {
            // Error retrieving data
        }

    }

    getActivities() {
        axios.get('http://192.168.1.30:8000/api/current-activity?user_id=' + this.state.user.id)
            .then(res => {
                if (res.length > 0)
                this.setState({
                    activities: res,
                })
            })
    }
    

    render() {

        const { navigation } = this.props;
        const user = navigation.getParam('user');

        return (

            <ScrollView style={styles.container}>

                <Header user={user} />

                <Card>
                    <CardItem styles={{backgroundColor:'#fff'}}>
                        <Input 
                            image={IMAGE_SOURCE}
                            placeholder="What's your Mood ? "
                            secureTextEntry={false}
                        />
                    </CardItem>
                    <CardItem styles={{backgroundColor:'#fff',borderColor: '#ddd',borderBottomWidth: 1 }}>
                        <CardItem>
                            <TouchableOpacity><Image source={HAPPY} style={styles.imageStyle} /></TouchableOpacity>
                            <TouchableOpacity><Image source={SAD} style={styles.imageStyle} /></TouchableOpacity>
                            <TouchableOpacity><Image source={ANGRY} style={styles.imageStyle} /></TouchableOpacity>
                            <TouchableOpacity><Image source={LOVE} style={styles.imageStyle} /></TouchableOpacity>
                        </CardItem>
                        <Button>Share</Button>
                    </CardItem>
                </Card>

                <Text style={styles.header}>Stories</Text>
                <CardItem styles={{backgroundColor:'#fff',borderWidth:0,marginBottom:2,paddingBottom:15}}>
                    {
                        this.state.activities.length ? this.state.activities.map(activity => {
                            return <Story key={activity.id} profileName={activity.name} profileImage={profile1} moodImage={mood1}/>
                        }) : null
                    }
                    {/*<Story profileName={'Person2'} profileImage={profile2} moodImage={mood2} />*/}
                    {/*<Story profileName={'Person3'} profileImage={profile3} moodImage={mood3} />*/}
                    {/*<Story profileName={'Person4'} profileImage={profile4} moodImage={mood4} />*/}
                </CardItem>
                {console.log(this.props)}
                <Card styles={{backgroundColor:'#fff', padding:5, borderRadius:0,paddingTop:15}}>
                    <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('Gift', {
                            id: 1,
                            name: 'Ahmed',
                        })}}>
                        <ActivityCard image={profile1} text={'Hamza Nabil'} text2={'4 min ago'} styles={{backgroundColor:'#fff',borderColor:'#01b1f7', borderWidth:1}}/>
                    </TouchableOpacity>
                    <ActivityCard image={profile2} text={'Noran Magdi'} text2={'3 min ago'} styles={{backgroundColor:'#fff',borderColor:'#01b1f7', borderWidth:1}}/>
                    <ActivityCard image={profile3} text={'Ahmed Ammar'} text2={'2 min ago'} styles={{backgroundColor:'#fff',borderColor:'#01b1f7', borderWidth:1}}/>
                </Card>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#FFF'
    },
    imageStyle: {
        width: 30,
        height:30,
        borderRadius: 30,
        justifyContent:'flex-start',
        paddingRight:10
    },
    header: {
        fontWeight:'bold',
        color:'#000',
        fontSize: 15, 
        paddingLeft: 10,
        paddingTop:15,
        backgroundColor:'#fff'
    }
});

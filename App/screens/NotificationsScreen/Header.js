import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Text, Dimensions, View, Image} from "react-native";

const { width } = Dimensions.get('window');

export default class Header extends React.Component {

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.coverContainer}>
                    <Image source={{
                        uri: 'https://i.pinimg.com/originals/8e/da/e2/8edae296029e18f896064ec9d9d848aa.jpg',
                    }} style={styles.coverImage} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        // backgroundColor: 'red',
        flexDirection: 'row',

        alignSelf: 'center',
        justifyContent:'center',
        width: '100%',
        overflow: 'hidden', // for hide the not important parts from circle
        height: 180
    },
    coverContainer: {
        height: '100%',
        width: '100%',
    },
    coverImage: {
        width: '100%',
        height: '100%',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
});

import React from 'react';
import PropTypes from 'prop-types';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import { Card ,CardItem , ActivityCard} from '../../components/index';
import axios from 'axios';
const profile1 = require('../../assets/images/person1.jpg')
const profile2 = require('../../assets/images/person2.jpg')
const profile3 = require('../../assets/images/person3.png')
const profile4 = require('../../assets/images/person4.png')

export default class NotificationsScreen extends React.Component {

    static propTypes = {
        navigation: PropTypes.object,
    };

    constructor(props) {
        super(props);
        state = {
            notifications: []
        };
    }
    

    static navigationOptions = {
         title: 'Notifications',
       // header: null,
    };

    componentWillMount() {
        this.getNotifications();
    }

    getNotifications() {
        axios.get('http://192.168.1.30:8000/api/notifications?user_id=1')
            .then(res => {
                console.log('resresresres',  res.data.data)
                this.setState({
                    notifications: res.data.data,
                })
            })
    }


    render() {


        return (
            <ScrollView style={styles.container}>
                 <Card styles={{backgroundColor:'#fff', padding:5, borderRadius:0,paddingTop:15}}>
                    <ActivityCard image={profile1} text={'Hamza Nabil'} text2={'Angry'} styles={{backgroundColor:'#fff',borderColor:'#01b1f7', borderWidth:1}}/>
                    <ActivityCard image={profile2} text={'Noran Magdi'} text2={'Sad'} styles={{backgroundColor:'#fff',borderColor:'#01b1f7', borderWidth:1}}/>
                    <ActivityCard image={profile3} text={'Ahmed Ammar'} text2={'Happy'} styles={{backgroundColor:'#fff',borderColor:'#01b1f7', borderWidth:1}}/>
                </Card>


            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#eee'

    },
    imageContainer: {
        width: 55,
        height: 55,
        // marginHorizontal: 10,
        borderRadius: 50,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    profileImage: {
        width: 55,
        height: 55,
        overflow: 'hidden',
    },
});

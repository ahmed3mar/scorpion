import React from 'react';
import {createBottomTabNavigator, createStackNavigator} from 'react-navigation';

import HomeScreen from './HomeScreen';
import SignIn from './SignIn';
import SignUp from './SignUp';
import NavigationService from "../services/Navigation";
import FriendsScreen from "./FriendsScreen";
import ProfileScreen from "./ProfileScreen";
import MyGiftsScreen from "./MyGiftsScreen";
import NotificationsScreen from "./NotificationsScreen";
import GiftScreen from "./GiftScreen";
import Ionicons from 'react-native-vector-icons/Ionicons';

const Routes = {
    SignIn: SignIn,
    SignUp: SignUp,
};

const Home = createStackNavigator({
    Home: HomeScreen,
});

const Gifts = createStackNavigator({
    Gifts: MyGiftsScreen,
    Gift: GiftScreen,
});

const Notifications = createStackNavigator({
    Notifications: NotificationsScreen,
});

const Friends = createStackNavigator({
    Friends: FriendsScreen,
    Profile: ProfileScreen,
});

const TabScreens = createBottomTabNavigator(
    {
        Home,
        Gifts,
        Friends,
        Notifications,
    },
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Home') {
                    iconName = `ios-home${focused ? '' : ''}`;
                } else if (routeName === 'Gifts') {
                    iconName = `ios-gift${focused ? '' : ''}`;
                } else if (routeName === 'Friends') {
                    iconName = `ios-contact${focused ? '' : ''}`;
                } else if (routeName === 'Notifications') {
                    iconName = `ios-information-circle${focused ? '' : '-outline'}`;
                }

                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Ionicons name={iconName} size={horizontal ? 20 : 25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: '#01b1f7',
            inactiveTintColor: 'gray',
        },
    }
);

const Application = createStackNavigator({
    SignIn: SignIn,
    SignUp: SignUp,
    Tabs: TabScreens,
}, {
    navigationOptions: {
        header: null,
    },
    cardStyle: {
        // backgroundColor: 'transparent'
    },
});

export default class App extends React.Component {
    render() {
        return (
            <Application ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef);
            }} />
        );
    }
}

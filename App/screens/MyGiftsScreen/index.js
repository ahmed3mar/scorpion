import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import RoundImage from "../../components/RoundImage";


export default class MyGiftsScreen extends React.Component {


    static navigationOptions = {
        title: 'My Gifts',
    };

    state = {
        search: '',
        gifts: []
    };

    componentWillMount() {
        this.getGifts();
    }

    getGifts() {
        axios.get('http://192.168.1.30:8000/api/my-gifts?user_id=1')
            .then(res => {
                console.log('resresresres', res)
                this.setState({
                    gifts: res.data.data,
                })
            })
    }

    renderGifts() {
        return this.state.gifts.map(gift => {
            return (
                <TouchableOpacity key={gift.id} style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 20,
                }} onPress={() => {
                    this.props.navigation.navigate('Gift', {
                        gift: gift,
                    })
                }}>
                    <RoundImage image={gift.user.image ? gift.user.image : 'https://cdn-images-1.medium.com/fit/c/72/72/1*KgtWLc0fl1TDbEVUJGqtgQ.jpeg'}/>
                    <View>
                        <Text style={{
                            fontSize: 15,
                            fontWeight: 'bold',
                        }}>{gift.user.name}</Text>
                        <Text style={{
                            color: '#afafaf',
                        }}>{gift.date}</Text>
                    </View>
                </TouchableOpacity>
            )
            // if (!friend.name.includes(this.state.search)) return null;
            // return <FriendCard friend={friend} key={friend.id} />
        });
    }

    render() {

        return (
            <View style={styles.container}>

                <ScrollView>
                    {
                        this.renderGifts()
                    }
                </ScrollView>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        // padding: 10,
        // marginTop: 20,
    }
});

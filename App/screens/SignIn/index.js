import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, AsyncStorage} from "react-native";
import Icon from 'react-native-vector-icons//Ionicons';
import { logIn } from "../../modules/auth";
import { connect } from "react-redux";
import {bindActionCreators} from "redux";

class SignIn extends React.Component {

    static propTypes = {
        navigation: PropTypes.object,
    };

    static navigationOptions = {
        // title: 'Gift',
        header: null,
    };

    constructor(props){
        super(props)
        this.state = {
            email: 'rana@gmail.com',
            password: '12345678',
            //  disabled: true,
        }
        this._onPressButton = this._onPressButton.bind(this);
    }

    async componentWillMount() {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value !== null) {
                // We have data!!
                this.props.navigation.navigate('Home', {
                    user: JSON.parse(value)
                });
            }
        } catch (error) {
            // Error retrieving data
        }
    }

    _onPressButton(){
        axios.post('http://192.168.1.30:8000/api/login', {
            email: this.state.email,
            password: this.state.password,
        })
            .then(res => {
                if (res.data) {
                    AsyncStorage.setItem('@user', JSON.stringify(
                        res.data
                    ));
                    this.props.navigation.navigate('Home', {
                        user: res.data
                    });
                } else {
                    alert(res);
                }
            })
    }

    render() {

        return (
            <View style={styles.container}>

                <View style={{flex:1,flexDirection:'column',margin:20,justifyContent:'center'}}>

                    <Icon name="ios-lock" size={80} color="#4F8EF7" style={{marginBottom:20,marginLeft:100 }} />
                    <TextInput
                        placeholder="Email"
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                        style={{borderColor: '#999999', borderBottomWidth: 1}}>
                    </TextInput>
                    <TextInput
                        placeholder="password"
                        style={{borderColor: '#999999', borderBottomWidth: .5,marginTop:10}}
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}>
                    </TextInput>
                    <TouchableOpacity onPress={this._onPressButton}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>Sign In</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {this.props.navigation.navigate('SignUp')}} >
                        <Text style={{marginTop:10,marginLeft:3}}>Don't have an account?</Text>
                    </TouchableOpacity>
                </View>


            </View>
        )
    }
}

export default connect(state => ({
    ali: state.auth.logged_in
}), dispatch => ({
    logIn: bindActionCreators(logIn, dispatch)
}))(SignIn)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor:'#fff'
    },
    button: {
        // marginBottom: 30,
        borderRadius: 3,
        marginTop: 30,
        width: 260,
        alignItems: 'center',
        backgroundColor: '#2196F3'
    },
    buttonText: {
        padding: 15,
        color: 'white'
    }

});

import React from 'react';
import PropTypes from 'prop-types';
import {Image, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Header from "./Header";
import Icon from "react-native-vector-icons/Ionicons";

export default class ProfileScreen extends React.Component {

    static propTypes = {
        navigation: PropTypes.object,
    };

    static navigationOptions = {
        // title: 'Gift',
        header: null,
    };

    constructor(props) {
        super(props);

    }

    state = {

    };


    render() {

        const { navigation } = this.props;
        const user = navigation.getParam('user');

        return (
            <View style={styles.container}>

                <Header />

                <View style={{
                    alignItems: 'center',
                    marginTop: -30,
                }}>
                    <View style={{
                        backgroundColor: 'white',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 70,
                        height: 70,
                        borderRadius: 70,
                    }}>
                        <View style={{
                            backgroundColor: 'green',
                            alignItems: 'center',
                            justifyContent: 'center',
                            width: 63,
                            height: 63,
                            borderRadius: 63,
                        }}>
                            <View style={{
                                backgroundColor: 'white',
                                alignItems: 'center',
                                justifyContent: 'center',
                                width: 60,
                                height: 60,
                                borderRadius: 60,
                            }}>
                                <View style={styles.imageContainer}>
                                    <Image style={styles.profileImage} source={{ uri: user.image ? user.image : 'https://cdn-images-1.medium.com/fit/c/100/100/1*aEzxzaWhlxp7ncnhj8URXg.jpeg' }} />
                                </View>
                            </View>
                        </View>
                    </View>
                    <Text style={{
                        marginTop: 5,
                        fontWeight: 'bold',
                    }}>{user.name}</Text>
                </View>

                <View style={{
                    marginTop: 10,
                    alignItems: 'center',
                }}>
                    <TouchableOpacity style={{
                        width: 150,
                        height: 30,
                        // backgroundColor: '#0084ff',
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row',
                        borderRadius: 2,
                        borderWidth: 1,
                        borderColor: '#0084ff',
                    }}>
                        <Icon style={{
                            fontSize: 20,
                            marginLeft: 5,
                            position: 'absolute',
                            left: 5,
                            color: '#0084ff',
                        }} name={'md-add'} />
                        <Text style={{
                            // color: 'white',
                            color: '#0084ff',
                            flex: 1,
                            textAlign: 'center',
                        }}>Add friend</Text>
                    </TouchableOpacity>
                </View>



                <View style={{
                    position: 'absolute',
                    top: 20,
                    left: 0,
                    // right: 20,
                }}>
                    <TouchableOpacity style={{
                        paddingLeft: 20,
                        paddingRight: 20,
                    }} onPress={() => {
                        this.props.navigation.goBack();
                    }}>
                        <Icon color={'#93236d'} size={25} name={'md-arrow-back'} />
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',

    },
    imageContainer: {
        width: 55,
        height: 55,
        // marginHorizontal: 10,
        borderRadius: 50,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    profileImage: {
        width: 55,
        height: 55,
        overflow: 'hidden',
    },
});

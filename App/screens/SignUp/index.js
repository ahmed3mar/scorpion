import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Text, View, TextInput, TouchableOpacity} from "react-native";
import Icon from 'react-native-vector-icons//Ionicons';

export default class SignUp extends React.Component {

    static propTypes = {
        navigation: PropTypes.object,
    };

    static navigationOptions = {
        // title: 'Gift',
        header: null,
    };

    constructor(props){
        super(props)
        this.state = {
            userName: '',
            password: '',
            email:'',
        }
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){
 console.log("this.state",this.state)
 
    }

    render() {

        return (
            <View style={styles.container}>
            
            <View style={{flex:1,flexDirection:'column',margin:20,justifyContent:'center'}}>
            
            <Icon name="md-person-add" size={80} color="#4F8EF7" style={{marginBottom:20,marginLeft:90 }} />
              <TextInput 
                    placeholder="userName"
                    onChangeText={(userName) => this.setState({userName})}
                    value={this.state.userName}
                    style={{borderColor: '#999999', borderBottomWidth: 1}}>
                    </TextInput>
                    <TextInput 
                    placeholder="Email"
                    onChangeText={(email) => this.setState({email})}
                    value={this.state.email}
                    style={{borderColor: '#999999', borderBottomWidth: 1}}>
                    </TextInput>
              <TextInput
               placeholder="password"
               style={{borderColor: '#999999', borderBottomWidth: .5,marginTop:10}}
               onChangeText={(password) => this.setState({password})}
               value={this.state.password}>
               </TextInput>
               

               <TouchableOpacity onPress={this._onPressButton}>
               <View style={styles.button}>
               <Text style={styles.buttonText}>Sign Up</Text>
                </View>
               </TouchableOpacity>
            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor:'#e6e6e6'
    },
    button: {
      //  marginBottom: 30,
        marginTop: 30,
        width: 260,
        alignItems: 'center',
        backgroundColor: '#2196F3'
      },
      buttonText: {
        padding: 15,
        color: 'white'
      }

});

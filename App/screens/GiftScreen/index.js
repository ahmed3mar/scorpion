import React from 'react';
import PropTypes from 'prop-types';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View ,Image} from "react-native";
import { Input, CardItem , Card , ActivityCard } from '../../components/index'
import RoundImage from "../../components/RoundImage";

import Confetti from 'react-native-confetti';
const IMAGE_SOURCE = require('../../assets/images/person1.jpg')
const mood3 = require('../../assets/images/mood3.jpg')
const profile3 = require('../../assets/images/person3.png')
export default class GiftScreen extends React.Component {

    static navigationOptions = {
        title: 'Gift',
    };

    componentDidMount() {
        if(this._confettiView) {
           this._confettiView.startConfetti();
        }
    }

    render() {

        const { navigation } = this.props;
        const gift = navigation.getParam('gift');

        return (
            <View style={styles.container}>
                <Confetti ref={(node) => this._confettiView = node}/>
                <View style={{
                    marginTop: 10,
                    flexDirection: 'column',
                    justifyContent:'center',
                    alignItems: 'center',
                }}>
                    <RoundImage image={gift.user.image ? gift.user.image : 'https://cdn-images-1.medium.com/fit/c/72/72/1*KgtWLc0fl1TDbEVUJGqtgQ.jpeg'}/>
                    <View>
                        <Text style={{
                            fontSize: 17,
                            fontWeight: 'bold',
                            textAlign:'center'
                        }}>{gift.user.name} Sent you a gift</Text>
                        <Text style={{
                            color: '#afafaf',
                            fontWeight:'bold',
                            fontSize: 15,
                            textAlign:'center'
                        }}>{gift.date} </Text>
                    </View>

                    <View>
                        <Image source={require('../../assets/images/gift.png')} style={{width:150,height:150,justifyConent:'center',marginTop:30}} />
                    </View>

                    
                     
                </View>
                <Card styles={{justifyConent:'flex-start'}}>
                    <CardItem styles={{backgroundColor:'#fff'}}>
                        <Input 
                            image={IMAGE_SOURCE}
                            placeholder="Write Your Comment"
                            secureTextEntry={false}
                        />
                    </CardItem>
                </Card>
                <ActivityCard image={profile3} text={'Ahmed Ammar'} text2={'2 min ago'} styles={{backgroundColor:'#fff',borderColor:'#eee', borderWidth:1}}/>
                <ActivityCard image={profile3} text={'Ahmed Ammar'} text2={'2 min ago'} styles={{backgroundColor:'#fff',borderColor:'#eee', borderWidth:1}}/>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        // padding: 10,
        // marginTop: 20,
    }
});

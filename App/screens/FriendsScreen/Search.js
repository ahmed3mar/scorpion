import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, TextInput, View, Image} from "react-native";
import Icon from 'react-native-vector-icons/dist/Ionicons';


export default class Search extends React.Component {

    render() {

        return (
            <View style={styles.container}>
                <Icon size={25} name={'md-search'} style={styles.searchIcon} />
                <TextInput
                    onChangeText={(val) => {
                        this.props.onChange(val);
                    }}
                    style={styles.searchInput}
                    placeholder={'Search'}
                    placeholderTextColor={'#A3A3A3'}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 40,
        alignItems: 'center',
        backgroundColor: '#F4F4F2',
        flexDirection: 'row',
        borderRadius: 10,
        marginBottom: 10,
    },
    searchIcon: {
        color: '#A3A3A3',
        marginLeft: 15,
    },
    searchInput: {
        paddingHorizontal: 10,
    },
    userImage: {
        width: 40,
        height: 40,
        overflow: 'hidden',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
});

import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Text, TouchableOpacity, View, Image} from "react-native";


export default class Header extends React.Component {

    render() {

        const { user } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <Image source={{
                        uri: user.image ? user.image : 'https://cdn.dribbble.com/users/527271/avatars/normal/e9428746748cca0e57e23f37ec25b787.jpg?1532390607',
                    }} style={styles.userImage} />
                </View>
                <Text style={styles.title}>{user.name}</Text>
                {/*<TouchableOpacity>*/}
                    {/*<Text>TEST</Text>*/}
                {/*</TouchableOpacity>*/}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 70,
        alignItems: 'center',
        // backgroundColor: 'red',
        flexDirection: 'row',
        paddingTop: 10,
        paddingLeft: 10,
    },
    imageContainer: {
        width: 40,
        height: 40,
        marginHorizontal: 10,
        borderRadius: 50,
        overflow: 'hidden',
    },
    userImage: {
        width: 40,
        height: 40,
        overflow: 'hidden',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        width: 200,
    },
});

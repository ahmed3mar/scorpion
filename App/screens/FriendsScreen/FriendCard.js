import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Text, TouchableOpacity, View, Image} from "react-native";


export default class FriendCard extends React.Component {

    renderOnline() {
        const whb = 10;
        const { friend } = this.props;
        return <View style={{
            backgroundColor: friend.online ? '#61CB49' : '#9d1c00',
            width: whb,
            height: whb,
            borderRadius: whb,
            marginRight: 10,
        }} />
    }

    render() {

        const { friend } = this.props;

        console.log('friendfriendfriendfriend', friend)

        return (
            <TouchableOpacity onPress={this.props.onPress} style={styles.container}>
                <View style={{
                    width: 63,
                    height: 63,
                    marginHorizontal: 10,
                    borderRadius: 70,
                    backgroundColor: friend.color,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <View style={{
                        width: 60,
                        height: 60,
                        marginHorizontal: 10,
                        borderRadius: 60,
                        backgroundColor: 'white',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                        <View style={styles.imgContainer}>
                            <Image source={{ uri: friend.image ? friend.image : 'https://i.pinimg.com/originals/8e/da/e2/8edae296029e18f896064ec9d9d848aa.jpg' }} style={styles.image} />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={styles.username}>{friend.name}</Text>
                    <Text style={styles.mood}>{friend.current_mood.name} . {friend.date}</Text>
                </View>
                {this.renderOnline()}
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 70,
        alignItems: 'center',
        // backgroundColor: 'red',
        flexDirection: 'row',
        marginTop: 5,
    },
    imgContainer: {
        width: 55,
        height: 55,
        marginHorizontal: 10,
        borderRadius: 50,
        overflow: 'hidden',
    },
    image: {
        width: 55,
        height: 55,
        overflow: 'hidden',
    },
    username: {
        fontWeight: 'bold',
        fontSize: 15,
    },
    mood: {
        color: '#B6B5B3',
    },
});

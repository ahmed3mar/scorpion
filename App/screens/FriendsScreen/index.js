import React from 'react';
import PropTypes from 'prop-types';
import {AsyncStorage, ScrollView, StyleSheet, Text, View} from "react-native";
import Header from "./Header";
import Search from "./Search";
import FriendCard from "./FriendCard";
import axios from "axios/index";

// import { logIn } from "../../modules/auth";

// @connect( state => ({
//
// }), () => ({
//     logIn: logIn,
// }) )
export default class FriendsScreen extends React.Component {

    static propTypes = {
        navigation: PropTypes.object,
    };

    static navigationOptions = {
        // title: 'Gift',
        header: null,
    };

    constructor(props) {
        super(props);

        this.onSearch = this.onSearch.bind(this);
    }

    onSearch(search) {
        this.setState({
            search
        })
    }

    state = {
        search: '',
        user: {},
        friends: []
    };

    async componentWillMount() {
        try {
            const value = await AsyncStorage.getItem('@user');
            if (value !== null) {
                // We have data!!
                this.setState({
                    user: JSON.parse(value)
                }, () => {
                    this.getGifts();
                })
            }
        } catch (error) {
            // Error retrieving data
        }

    }

    getGifts() {
        axios.get('http://192.168.1.30:8000/api/friends-list?user_id=' + this.state.user.id)
            .then(res => {
                console.log('resresresresres', res)
                this.setState({
                    friends: res.data.data,
                })
            })
    }

    renderFriends() {
        return this.state.friends.map(friend => {
            if (!friend.name.includes(this.state.search)) return null;
            return <FriendCard onPress={() => {
                this.props.navigation.navigate('Profile', {
                    user: friend,
                })
            }} friend={friend} key={friend.id} />
        });
    }

    render() {

        return (
            <View style={styles.container}>
                <Header user={this.state.user} />
                <Search onChange={this.onSearch} />

                <ScrollView>
                    {
                        this.renderFriends()
                    }
                </ScrollView>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
    }
});

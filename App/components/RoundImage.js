import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Text, TouchableOpacity, View, Image} from "react-native";


export default class RoundImage extends React.Component {

    render() {

        const { image } = this.props;

        return (
            <View style={styles.imgContainer}>
                <Image source={{ uri: image }} style={styles.image} />
            </View>
        )
    }
}

const styles = StyleSheet.create({

    imgContainer: {
        width: 55,
        height: 55,
        marginHorizontal: 10,
        borderRadius: 50,
        overflow: 'hidden',
    },
    image: {
        width: 55,
        height: 55,
        overflow: 'hidden',
    },
});

import React from 'react';
import { View, StyleSheet } from 'react-native';


const styles = StyleSheet.create({
  cardItem: {
    padding: 3,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});

const CardItem = (props) => {
  return(
    <View style={[styles.cardItem, props.styles]}>
      { props.children }
    </View>
  );
};


export { CardItem };

import React from 'react';
import { View, StyleSheet , Image , Text} from 'react-native';


const styles = StyleSheet.create({
  cardItem: {
    flex:1,
    padding: 5,
    flexDirection: 'row',
    shadowColor: '#eee',
    shadowOpacity: 0.5,
    justifyContent: 'flex-start',
    marginBottom : 8,
    borderRadius:5,
    marginRight:2,
    marginLeft:2
  },
  text: {
      color:'#000',
      fontSize: 15,
      fontWeight:'bold',
      paddingTop:10,
      paddingLeft: 20
  },
  imagePreview:{
      width: 50,
      height: 50,
      borderRadius: 50,
      marginTop:5
  },
  text2: {
     color:'#000',
     fontSize:13,
     paddingLeft: 20
  }
});

const ActivityCard = (props) => {
  return(
    <View style={[styles.cardItem, props.styles]}>
        <Image source={props.image} style={styles.imagePreview}/>
        <View>
            <Text style={styles.text}>{props.text}</Text>
            <Text style={styles.text2}>{props.text2}</Text>
        </View>
    </View>
  );
};


export { ActivityCard };

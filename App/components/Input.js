import React from 'react';
import { Text, TextInput, View, StyleSheet , Image} from 'react-native';


const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems : 'center',
    
  },
  label: {
    fontSize: 16,
    paddingLeft: 20,
    flex: 1
  },
  input: {
    fontSize: 16,
    color: '#000',
    paddingLeft: 10,
    paddingRight: 5,
    marginRight:10,
    flex: 2,
    borderWidth:1,
    borderColor:'#ccc',
    borderRadius:30
  },
  imageProgile: {
      width: 50,
      height: 50,
      marginBottom: 20,
      marginRight: 10,
      marginTop:20,
      borderRadius: 50,
      borderWidth:2,
      borderColor: '#ccc',
  }

});

const Input = (props) => {
  return (
    <View style={styles.inputContainer}>

      <Image source={props.image} style={styles.imageProgile}/>

      <TextInput
        placeholder={props.placeholder}
        secureTextEntry={props.secureTextEntry}
        autoCorrect={false}
        autoCapitalize='none'
        onChangeText={props.onChangeText}
        style={styles.input} />
    </View>
  );
};


export { Input }

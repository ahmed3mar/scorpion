import React from 'react';
import { View, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  cardStyle: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,
    borderRadius: 20,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#ccc',
    shadowOpacity: 0.2,
    marginBottom: 10, 
  }
});


const Card = (props) => {
  return(
    <View style={[styles.cardStyle, props.styles]}>
      { props.children }
    </View>
  );
};

export { Card };

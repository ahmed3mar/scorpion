import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  button: {
      height: 40,
      borderRadius: 5,
      backgroundColor: '#01b1f7',
      justifyContent: 'center',
      width:100
    },
    buttonText: {
      color: '#fff',
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 15,
    }
});

const Button = (props) => {
  return (
    <TouchableOpacity style={styles.button} onPress={props.onPress}>
      <Text style={styles.buttonText}>
        { props.children }
      </Text>
    </TouchableOpacity>
  );
}

export { Button };

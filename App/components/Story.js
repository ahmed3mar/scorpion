import React from 'react';
import { TouchableOpacity, Text, StyleSheet , View , Image} from 'react-native';

const styles = StyleSheet.create({
    story: {
      height: 40,
      borderRadius: 5,
      justifyContent: 'center',
      width:100,
      height:100
    },
    storyText: {
      color: '#000',
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 15,
    },
    wraperImage: {
        width:100,
        height:100,
        justifyContent: 'center',
        alignItems:'center',
        position:'relative'
    },
    mood: {
        width:70,
        height:70,
        borderRadius:70,
        borderWidth:1,
        borderColor:'#01b1f7',
        padding:2,
        backgroundColor:'#fff'
    },
    moodImage: {
        width:'100%',
        height:'100%',
        borderRadius:80
    },
    profil: {
        width:30,
        height:30,
        position:'absolute',
        bottom:10,
        borderWidth:1,
        borderColor:'#01b1f7',
        borderRadius:30,
        right:5,
        padding:1,
        backgroundColor:'#fff'
    },
    profilImage: {
        width:'100%',
        height:'100%',
        borderRadius:30
    },
    image: {
        padding:5
    }
});



const Story = (props) => {
  return (
    <TouchableOpacity style={styles.story} onPress={props.onPress}>
        <View style={styles.wraperImage}>
            <View style={styles.mood}><Image source={props.moodImage} style={styles.moodImage}/></View>
            <View style={styles.profil}><Image source={props.profileImage} style={styles.profilImage}/></View>
        </View>
        <Text style={styles.storyText}>
            { props.profileName }
        </Text>
    </TouchableOpacity>
  );
}

export { Story };
